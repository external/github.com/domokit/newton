# Changelog

## 0.1.2

- Add BoundedFrictionSimulation

## 0.1.1

- Allow setting tolerances on simulations


## 0.1.0

- Initial Version
